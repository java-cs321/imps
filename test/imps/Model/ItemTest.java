/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model;

import static imps.Model.Item.itemSearch;
import imps.Model.Storage.DBService;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import imps.Model.Storage.H2DB;
import imps.Model.Storage.MockDBService;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;

/**
 *
 * @author ewill
 * @author Michael Colsch
 */
public class ItemTest {
    
    public ItemTest() {
    }
    private Item item;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        item = new Item();
    }
    
    @After
    public void tearDown() {
        item = null;
    }

    /**
     * Test of addSupply method, of class Item.
     */
    @Test
    public void testAddQuantity() {
        int add = 1;
        item.addQuantity(add);
        assertEquals(1,item.getQuantity());
       
    }

    /**
     * Test of removeQuantity method, of class Item.
     */
    @Test
    public void testRemoveQuantity() {
        int minus = 1;
        item.setQuantity(2);
        item.removeQuantity(minus);
       assertEquals(1,item.getQuantity());
       
    }

    /**
     * Test of checkQuantity method, of class Item.
     */
    @Test
    public void testGetQuantity() {
        int expResult = 0;
        int result = item.getQuantity();
        assertEquals(expResult, result);
    }

    /**
     * Test of totalValue method, of class Item.
     */
    @Test
    public void testTotalValue() {
        float expResult = 0.0F;
        float result = item.totalValue().floatValue();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of itemSearch method, of class Item.
     */
    @Test
    public void testItemSearch() throws SQLException {
        MockDBService db = new MockDBService();
       db.IntReturn = 1;
        db.ItemArrayReturn = new ArrayList<Item>();
        Item.setDatabase(db);
        Item item1 = new Item("Bolts" ,1 ,new BigDecimal(2.99));
        db.ItemArrayReturn.add(item1);
        String test = "Bolts"; // TEST STRING EXAMPLE

        ArrayList<Item> result = itemSearch(test);
        assertEquals(result.size(),1);
        
    }

    /**
     * Test of setDatabase method, of class Item.
     */
    @Test
    public void testSetDatabase() {
        DBService db = null;
        Item.setDatabase(db);
        
    }

    /**
     * Test of setPrice method, of class Item.
     */
    @Test
    public void testSetPrice() {
        BigDecimal a = new BigDecimal(1.0);
        item.setPrice(a);
        assertTrue(a.equals(item.getPrice()));
       
    }

    /**
     * Test of setUnits method, of class Item.
     */
    @Test
    public void testSetUnits() {
        int a = 5;
        item.setQuantity(a);
       assertEquals(item.getQuantity(),5);
    }



    /**
     * Test of getPrice method, of class Item.
     */
    @Test
    public void testGetPrice() {
        BigDecimal expResult = new BigDecimal(0);
        BigDecimal result = item.getPrice();
        assertTrue(result.equals(expResult));
       
    }
    /**
     * Test of itemSort method, of class Item.
     * Tests an array list of hard coded data to be sorted alphabetically. 
     * After sorting it should be in the order; bolt, nail, wrench. 
     */
    @Test
    public void testItemSortAlpha() {
        MockDBService db = new MockDBService();
        item.setDatabase(db);
         
        ArrayList<Item> test = new ArrayList<Item>();
        Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
        Item wrench = new Item("Wrench", 2, new BigDecimal(15.99));
        Item nail = new Item("Nail", 45, new BigDecimal(.99));
        test.add(bolt);
        test.add(wrench);
        test.add(nail);
        ArrayList<Item> result = Item.itemSort(test,Item.SortBy.NAME);
        assertEquals(bolt,result.get(0));
        assertEquals(nail,result.get(1));
        assertEquals(wrench,result.get(2));
        
        assertNotEquals(result,test);
    }

    /**
     * Test of itemSort method, of class Item.
     * Tests an array list of hard coded data to be sorted by price. 
     * After sorting it should be in the order; nail, bolt, wrench. 
     */
    @Test
    public void testItemSortPrice() {
         MockDBService db = new MockDBService();
        Item.setDatabase(db);
        
       ArrayList<Item> test = new ArrayList<Item>();
        Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
        Item wrench = new Item("Wrench", 2, new BigDecimal(15.99));
        Item nail = new Item("Nail", 45, new BigDecimal(.99));
        test.add(bolt);
        test.add(wrench);
        test.add(nail);
        
        ArrayList<Item> result = Item.itemSort(test,Item.SortBy.PRICE);
        assertEquals(nail,result.get(0));
        assertEquals(bolt,result.get(1));
        assertEquals(wrench,result.get(2));
        
        assertNotEquals(result,test);
    }

    /**
     * Test of getName method, of class Item.
     */
    @Test
    public void testGetName() {
        Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
        assertEquals( "Bolt", bolt.getName());
        
    }

    /**
     * Test of setName method, of class Item.
     */
    @Test
    public void testSetName() {
       Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
       bolt.setName("Wrench");
       assertEquals("Wrench", bolt.getName());
    }

    /**
     * Test of setQuantity method, of class Item.
     */
    @Test
    public void testSetQuantity() {
        Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
         bolt.setQuantity(5);
        assertEquals(5,bolt.getQuantity());
    }

   

    /**
     * Test of getPicture method, of class Item.
     */
    @Test
    public void testGetPicture() {
       
    }

    /**
     * Test of setPicture method, of class Item.
     */
    @Test
    public void testSetPicture() {
       
    }

    
    
}
