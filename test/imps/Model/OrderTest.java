/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model;

import static imps.Model.Order.orderSearch;
import imps.Model.Storage.DBService;
import imps.Model.Storage.H2DB;
import imps.Model.Storage.MockDBService;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static imps.Model.Order.orderSortID;
import java.lang.reflect.Field;

/**
 *
 * @author ewill
 */
public class OrderTest {
    
    public OrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockDBService db = new MockDBService();
        Order.setDatabase(db);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setCode method, of class Order.
     */
    @Test
    public void testSetCode() {
        int code = 3;
        Order instance = new Order("En route", 1);
        instance.setCode(code);
      assertEquals(3, instance.getCode());
    }

    /**
     * Test of getCode method, of class Order.
     */
    @Test
    public void testGetCode() {

        Order instance = new Order("Test string", 20);
        int expResult = 20;
        int result = instance.getCode();
        assertEquals(expResult, result);
     
        
    }

    /**
     * Test of setorderID method, of class Order.
     */
    @Test
    public void testSetID() {
        
  
        int ID = 3;
        Order instance = new Order();
        instance.setID(ID);
       assertEquals(ID, instance.getID());
    }

    /**
     * Test of updateStatus method, of class Order.
     */
    @Test
    public void testUpdateCode() {
        int code = 2;
        Order instance = new Order();
        instance.updateCode(code);
      assertEquals(code, instance.getCode());
       
    }

    /**
     * Test of getStatus method, of class Order.
     */
    @Test
    public void testGetStatus() {
        MockDBService service = new MockDBService();
        Order.setDatabase(service);
        Order instance = new Order("Ordered",1);
        
        
        String expResult = service.toString();
        String result = instance.getStatus();
        assertEquals(expResult, result);
      
      
    }

    /**
     * Test of orderSearch method, of class Order.
     */
    @Test
    public void testOrderSearch() throws SQLException {
        MockDBService db = new MockDBService();
        
        try {
            db.Init("./imps-test");//Pass in dbName to Init function
        } catch (SQLException ex) {
          
        }
        Order.setDatabase(db);
        Order test = new Order("Ordered", 1);
        test.setID(1);
        
        db.OrderArrayReturn = new ArrayList<>();
        db.OrderArrayReturn.add(test);
        
        db.CreateOrder(test);
        String test_s = "Ordered"; // TEST STRING EXAMPLE
        ArrayList<Order> result;
        result = orderSearch(test_s);
        for(int i = 0; i<result.size(); i++){
        assertEquals("Ordered", 1 , result.get(i).getID());  
        }
    }

    /**
     * Test of setDatabase method, of class Order.
     */
    @Test
    public void testSetDatabase() {
        System.out.println("setDatabase");
        DBService db = null;
        Order.setDatabase(db);
        assertEquals(null, Order.getDatabase());
    }

    /**
     * Test of totalOrderValue method, of class Order.
     */
    @Test
    public void testTotalOrderValue() {
        System.out.println("totalOrderValue");
        Order instance = new Order();
        float expResult = 0.0F;
        BigDecimal result = instance.totalOrderValue();
        assertEquals(expResult, result.floatValue(), 0.0);
        
       
    }

    /**
     * @author Paul, moved from Driver.
     * Test of addToArray method, of class Order.
     */
    @Test
    public void testAddToArray() {
        Order test = new Order();
        
        test.addToArray(new Item("",2,new BigDecimal(5)));
        test.addToArray(new Item("",5,new BigDecimal(4)));
        test.addToArray(new Item("",3,new BigDecimal(10.75)));
        BigDecimal totalTest = test.totalOrderValue();
        assertTrue(totalTest.equals(new BigDecimal(62.25)));
      
    }

    /**
     * Test of orderSort method, of class Order.
     */
    @Test
    public void testOrderSortID() {
        
        MockDBService db = new MockDBService();
        Order.setDatabase(db);
      
        System.out.println("--UNIT TEST--");
        System.out.println("--ID sort--");
        ArrayList<Order> test = new ArrayList<Order>();
        Order order1 = new Order("Ordered", 1);
        order1.setID(5);
        Order order3 = new Order("Delivered", 3 );
        order3.setID(1);
        Order order2 = new Order("Finalized", 4);
        order2.setID(9);
        test.add(order1);
        test.add(order2);
        test.add(order3);
        test = orderSortID(test);
        assertEquals(1,test.get(0).getID());
        assertEquals(5,test.get(1).getID());
        assertEquals(9,test.get(2).getID());
        
    }

    /**
     * Test of getOrderID method, of class Order.
     */
    @Test
    public void testGetID() {
        System.out.println("getOrderID");
        Order instance = new Order();
        int expResult = 0;
        int result = instance.getID();
        assertEquals(expResult, result);
        
      
    }

   
    /**
     * Test of getTotal method, of class Order.
     */
    @Test
    public void testGetTotal() {
       Order order1 = new Order("Ordered", 1);
       Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
       order1.addToArray(bolt);
       BigDecimal expected = new BigDecimal(5.99).multiply(new BigDecimal(4));
       BigDecimal actual = order1.totalOrderValue();
       assertEquals(expected,actual );
        
    }
    /**
     * Test of getItems method, of class Order.
     */
    @Test
    public void testGetItems() {
        Order order1 = new Order("Ordered", 1);
         Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
         Item wrench = new Item("Wrench", 2, new BigDecimal(15.99));
         order1.addToArray(bolt);
         order1.addToArray(wrench);
         ArrayList<Item> expected = new ArrayList<Item>();
         expected.add(bolt);
         expected.add(wrench);
         assertEquals(expected, order1.getItems() );
         
         
    }

    /**
     * Test of setItems method, of class Order.
     */
    @Test
    public void testSetItems() {
        ArrayList<Item> expected = new ArrayList<Item>();
         Item bolt = new Item("Bolt", 4,new BigDecimal(5.99));
         Item wrench = new Item("Wrench", 2, new BigDecimal(15.99));
        expected.add(bolt);
        expected.add(wrench);
         Order order1 = new Order("Ordered", 1);
         order1.setItems(expected);
        assertEquals(expected, order1.getItems());
    }

    
}
