/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.Storage;

import imps.Model.Item;
import imps.Model.Order;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class that provides Integration Testing for H2DB (H2DB IT)
 * @author Michael Colsch
 */
public class H2DBIT {
    
    private H2DB db;
    
    public H2DBIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Clean up DB and then reinitialize one.
     */
    @Before
    public void setUp() {
        db = new H2DB();
        try {
            db.Init("./test.db");
            Connection conn = db.newConnection();
            db.CreateSchema(conn);
        }catch(Exception e){}
    }
    
    @After
    public void tearDown() {
        try {
            File file = new File("test.mv.db");
            file.delete();
            file = new File("test.trace.db");
            file.delete();
        }catch(Exception e){}
    }

    /**
     * Test of NewConnection method, of class H2DB.
     */
    @Test
    public void testNewConnection() throws Exception {
        System.out.println("NewConnection");
        H2DB instance = new H2DB();
        //Trickery to bypass java class protections, must be in same package.
        Field dbname = instance.getClass().getDeclaredField("dbName");
        dbname.setAccessible(true);
        dbname.set(instance, "./test");
        Connection expResult = null;
        Connection result = instance.newConnection();
        assertNotEquals(expResult, result);
        
    }

    /**
     * Test of CreateSchema method, of class H2DB.
     */
    @Test
    public void testCreateSchema() throws Exception {
        System.out.println("CreateSchema");
        
        H2DB instance = new H2DB();
        //Trickery to bypass java class protections, must be in same package.
        Field dbname = instance.getClass().getDeclaredField("dbName");
        dbname.setAccessible(true);
        dbname.set(instance, "./test");
        Connection db = instance.newConnection();
        instance.CreateSchema(db);
        Statement querier = db.createStatement();
        ResultSet results = querier.executeQuery("SELECT * from imps.ITEMS");
        //An exception is thrown if the column does not exist
        results.findColumn("ID");
        results.findColumn("Name");
        results.findColumn("Price");
        results.findColumn("Quantity");
        results.findColumn("Picture");
    }

    /**
     * Test of Init method, of class H2DB.
     * @throws Exception test failure
     */
    @Test
    public void testInit() throws Exception {
        System.out.println("Init");
        String name = "./test";
        //Testing this would be identical to CreateSchema at the moment.
        //Will be expanded if needed.
        H2DB instance = new H2DB();
        instance.Init(name);
        
    }

    /**
     * Test of GetItemsInInventory method, of class H2DB.
     */
    @Test
    public void testGetItemsInInventory() {
        System.out.println("GetItemsInInventory");
        H2DB instance = new H2DB();
        ArrayList<Item> expResult = new ArrayList<Item>();
        ArrayList<Item> result = instance.GetItemsInInventory();
        assertEquals(expResult, result);
        // TODO Fetch Items from inventory
        
    }

    /**
     * Test of GetOrders method, of class H2DB.
     */
    @Test
    public void testGetOrders() {
        System.out.println("GetOrders");
        H2DB instance = new H2DB();
        ArrayList<Order> expResult = new ArrayList<>();
        ArrayList<Order> result = instance.GetOrders();
        assertEquals(expResult, result);
        // TODO Fetch orders from inventory
        
    }

    /**
     * Test of GetItemsForOrder method, of class H2DB.
     */
    @Test
    public void testGetItemsForOrder() {
        System.out.println("GetItemsForOrder");
        int orderID = 1;
        Order o = new Order();
        o.setCode(1);
        o.setID(orderID);
        o.setName("name");
        Item item = new Item("n", 1, BigDecimal.ONE);
        ArrayList<Item> expResult = new ArrayList<>();
        expResult.add(item);
        o.addToArray(item);
        try {
            int id = db.CreateItem(item);
            item.setID(id);
            id = db.CreateOrder(o);
            o.setID(id);
        }catch(SQLException e){ fail(e.getMessage());}
        
        ArrayList<Item> result =  db.GetItemsForOrder(orderID);
        if (result == null){
            fail("Expected non-null result");
            return;
        }
        assertEquals(expResult.size(), result.size());
        // TODO ensure items match order table
        
    }

    /**
     * Test of CreateItem method, of class H2DB.
     * @throws Exception test failure
     */
    @Test
    public void testCreateItem() throws Exception {
        System.out.println("CreateItem");
        Item item = new Item("name",10,new BigDecimal(4));
        H2DB instance = new H2DB();
        instance.Init("./test");
        int expResult = 1;
        int result = instance.CreateItem(item);
        assertEquals(expResult, result);
        // TODO run select to test item creation
        
    }

    /**
     * Test of CreateOrder method, of class H2DB.
     * @throws Exception test failure
     */
    @Test
    public void testCreateOrder() throws Exception {
        System.out.println("CreateOrder");
        
        H2DB instance = new H2DB();
        Order.setDatabase(instance);
        instance.Init("./test");
        int expResult = 1;
        Order order = new Order("Test", 1);
        int result = instance.CreateOrder(order);
        assertEquals(expResult, result);
        // TODO run select to test order creation
        
    }
    
    /**
     * Test of UpdateOrder method, of class H2DB.
     * @throws SQLException test failure
     */
    @Test
    public void testUpdateOrder() throws SQLException{
        System.out.println("UpdateOrder");
        Order.setDatabase(db);
        Order order = new Order("Test", 1);
        int ID = db.CreateOrder(order);
        order.setID(ID);
        //Update the order.
        order.setName("Name");
        db.UpdateOrder(order);
        //Check the db.
        ArrayList<Order> orders = db.orderSearch(order.getName());
        assertEquals(1,orders.size());
    }

    /**
     * Test of AddItemsToOrder method, of class H2DB.
     * @throws Exception test failure
     */
    @Test
    public void testAddItemsToOrder() throws Exception {
        System.out.println("AddItemsToOrder");
        Order order = new Order("It order", 1);
        order.setID(1);
        ArrayList<Item> Items = new ArrayList<>();
        Items.add(new Item("Test", 0, BigDecimal.ONE));
        Items.get(0).setID(db.CreateItem(Items.get(0)));
        order.setID(db.CreateOrder(order));
                
        db.AddItemsToOrder(1, Items);
        
        ArrayList<Item> actual = db.GetItemsForOrder(order.getID());
        if (actual == null){
            fail("Expected non-null result.");
            return;
        }
        assertEquals(Items.size(),actual.size());
    }
    
    
    /**
     * Test getStatuses() method, of class H2DB
     */
    @Test
    public void testgetStatuses() throws SQLException{
        Map<Integer,String> actual = db.getStatuses();
        
        assertNotNull(actual);
        assertEquals(actual.get(1), "Pending");
    }
    
    
    /**
     * Test bytesToImage() function of class H2DB
     */
    @Test
    public void testBytesToImage(){
        try{
            db.Seed();
            db.itemSearch("I-Beams");
        }catch(SQLException | IOException | NullPointerException e){
            fail("Unexpected exception:\n"+e.getMessage());
        }
        
    }
    
}
