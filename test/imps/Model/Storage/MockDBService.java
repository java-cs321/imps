/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.Storage;

import imps.Model.Item;
import imps.Model.Order;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Mock Service that implements db service
 * Used for testing other modules
 * @author Michael Colsch
 */
public class MockDBService implements DBService {

    /**
     * These members are public to facilitate testing.
     */
    public ArrayList<Item> ItemArrayReturn;
    public ArrayList<Order> OrderArrayReturn;
    public boolean ThrowException;
    public int IntReturn;
    public Map<Integer,String> MapReturn;
    
    @Override
    public boolean Init(String name) throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return false;
    }

    @Override
    public ArrayList<Item> GetItemsInInventory() throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.ItemArrayReturn;
    }

    @Override
    public ArrayList<Order> GetOrders() throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.OrderArrayReturn;
    }

    @Override
    public ArrayList<Item> GetItemsForOrder(int orderID) throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.ItemArrayReturn;
    }

    @Override
    public int CreateItem(Item item) throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.IntReturn;
    }
    
    @Override
    public int UpdateItem(Item item) throws SQLException{
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.IntReturn;
    }

    @Override
    public int CreateOrder(Order order) throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.IntReturn;
    }
    
    @Override
    public int UpdateOrder(Order order) throws SQLException{
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.IntReturn;
    }

    @Override
    public void AddItemsToOrder(int OrderID, ArrayList<Item> Items) throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
    }

    @Override
    public ArrayList<Item> itemSearch(String search) {
        return this.ItemArrayReturn;
    }

    @Override
    public ArrayList<Order> orderSearch(String search) {
        return this.OrderArrayReturn;
    }

    @Override
    public String statusFromCode(int code) {
        return this.toString();
    }

    @Override
    public Map<Integer, String> getStatuses() throws SQLException {
        if (ThrowException) {
            throw new java.sql.SQLException("Test Method Exception");
        }
        return this.MapReturn;
    }


    
}
