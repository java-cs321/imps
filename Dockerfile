# Container that builds and runs the tests on this project

# Use super light-weight linux as the base container
FROM openjdk:12-alpine
# Install Java and Ant in this container
#RUN apk update && apk add openjdk8 && apk add apache-ant
RUN apk update && apk add apache-ant
RUN apk add xvfb libxrender libxtst libxi xorg-server
RUN apk add font-bitstream-type1
#RUN apk add xorg-server xf86-video-vesa xf86-input-evdev xf86-input-mouse xf86-input-keyboard udev

# Copy the project(current dir) to the container
COPY . .

ENV DISPLAY=:0
# Start the container by running the ant build command with a virtual window system. 
# Unit tests are ran as part of the build process.
CMD ./test.sh
