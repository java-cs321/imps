package imps.View.Item;

import imps.Controller.Item.CreateItemUpdateListener;
import imps.Model.Item;
import javax.swing.JSpinner;

/**
 * GUI Component for Displaying Quantity.
 * Adapter from JSpinner to CreateItemUpdateListener.
 * @author Michael Colsch
 */
public class ItemQuantitySpinner  implements CreateItemUpdateListener{

    
    JSpinner spinner;
    
    
    /**
     * Create an ItemQuantitySpinner from a JSpinner.
     * @param spinner to update.
     */
    public ItemQuantitySpinner(JSpinner spinner){
        this.spinner = spinner;        
        
    }
    /**
    * Notifies the GUI the quantity of the item has changed.
    * @param item this is the item being passed in with the changed data.
    */
    @Override
    public void ItemChanged(Item item) {
        if (item == null){
            spinner.setValue(0);      
        } else {
            spinner.setValue(item.getQuantity());
        }
        
    }
    
}
