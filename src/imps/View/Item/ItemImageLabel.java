
package imps.View.Item;

import imps.Controller.Item.CreateItemUpdateListener;
import imps.Controller.Item.ItemUpdateObserver;
import imps.Model.ItemImageLabelModel;
import imps.Model.Item;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * GUI Label that displays images. 
 * Adapter from JLabel to CreateItemUpdateListener
 * @author Michael Colsch
 */
public class ItemImageLabel extends ItemUpdateObserver implements CreateItemUpdateListener {

    // decorator pattern not used because then we wouldn't be able to place it using the designer
    
    private final JLabel label;
    
    private ItemImageLabelModel model;
    
    /**
     * Wraps a Label to turn it into a picture.
     * @param lbl label to wrap.
     */
    public ItemImageLabel(JLabel lbl) {
        label = lbl;
        label.setText("");
        
        model = new ItemImageLabelModel(lbl);
    }

    /**
     * Updates Image shown in label.
     * @param item that contains the new image to use.
     */
    @Override
    public void ItemChanged(Item item) {
        if (item == null || item.getPicture() == null){
            label.setIcon(null);
        }else {
            Image im = item.getPicture();
            label.setIcon(scaleImage(im));
        }
        this.model.setItem(item);
    }

    
    /**
     * Retrieves the model for this object.
     * @return model for the image label.
     */
    public ItemImageLabelModel getModel(){
        return model;
    }
    
    /**
     * Scales the image to match the label
     * @param image to scale.
     * @return ImageIcon with scaled image
     */
    private ImageIcon scaleImage(Image im){
        
        Image resizedImg = im.getScaledInstance(label.getWidth(), label.getHeight(), java.awt.Image.SCALE_SMOOTH);
        
        return new ImageIcon(resizedImg);
    }
    
    
    
    
   
    
    
}
