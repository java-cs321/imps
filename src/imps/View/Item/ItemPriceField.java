
package imps.View.Item;

import imps.Controller.Item.CreateItemUpdateListener;
import imps.Model.Item;
import javax.swing.JFormattedTextField;

/**
 * GUI Component for displaying price.
 * Uses the adapter pattern on JFormattedTextFields.
 * @author Michael Colsch
 */
public class ItemPriceField implements CreateItemUpdateListener{
    
    private JFormattedTextField field;
    
    //Item used as a hook for this object.
    private Item item;
    
    /**
     * Create an adapter that binds a JFormatted text field to an item's price.
     * @param field to bind to.
     */
    public ItemPriceField(JFormattedTextField field){
        this.field = field;
        
    }
    /**
    * This method changes the price to the item's new price.
    * @param item this is the item with the new price
    */
    @Override
    public void ItemChanged(Item item) {
       if (item == null || item.getPrice() == null){
           field.setText("");
       }else {
           field.setText(item.getPrice().toString());
       }
    }
    
    
}
