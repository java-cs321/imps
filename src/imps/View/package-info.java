/**
 * Package for controlling graphical elements.
 * Manages what the user sees.
 * Distributed into sub-packages for Item and Order.
 * The main package controls the root visual elements.
 */
package imps.View;
