/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller;
import imps.Controller.Item.CreateItemController;
import imps.Controller.Order.CreateOrderController;
import imps.Model.Item;
import imps.Model.Order;
import imps.Model.TableModel.ItemTableModel;
import imps.Model.TableModel.OrderTableModel;
import imps.View.Item.ItemEditorPanel;
import imps.View.Order.OrderEditorPanel;
import java.util.ArrayList;
import imps.View.MainPanel;
import imps.View.MainPanel.View;
import javax.swing.JTable;
/**
 * Main Controller Class, funnels data from the Model to the View.
 * @author Michael Colsch
 */
public class Controller {
    
    public static Controller Instance;
    
   //ItemEditor Controller
    private CreateItemController createItemController;
        
    //MainPanel to use for navigation.
    private MainPanel panel;
    
    private CreateOrderController createOrderController;
    
    /**
     * Observer used to notify when DB gets updated.
     */
    public Controller(){
                
    }
    
    /**
     * Returns an observer that will notify listeners when an item is created or updated.
     * @return observer that watches for item changes
     */
    public DatabaseUpdateObserver getDBItemObserver(){
       return createItemController;
    }
    
    /**
     * Returns an observer that will notify listeners when an order is created or updated.
     * @return observer that notifies on order change.
     */
    public DatabaseUpdateObserver getDBOrderObserver(){
        return createOrderController;
    }
    

    /**
     * Initializes the CreateItemController with it's matching view.
     * @param view to bind to the controller.
     */
    public void InitCreateItemController(ItemEditorPanel view){
        createItemController = new CreateItemController(view);
    }
    
    
    /**
     * Binds navigation to the controller
     * @param panel that holds navigation
     */
    public void setMainView(MainPanel panel){
        this.panel = panel;
    }
    
    
    /**
     * Navigation pass through function for the controller.
     * @param view to navigate to.
     */
    public void navigate(View view){
        panel.navigate(view);
    }
    
    
    /**
     * Initializes the CreateOrderController with it's matching view.
     * @param view to bind to the controller.
     */
    public void InitCreateOrderController(OrderEditorPanel view){
        createOrderController = new CreateOrderController(view);
        //Links OrderController's itemList to the ItemObserver.
        this.getDBItemObserver().addListener(createOrderController);
    }
    
    
    
    
    /**
     * Returns a list of items using search term.
     * @param search string to filter by
     * @return ItemTableModel built from list.
     */
    public ItemTableModel ListItems(String search){
        return new ItemTableModel(Item.itemSearch(search));
        // ItemTable.update();
    }
    
    /**
     * Returns a list of orders using search term.
     * @param search string to filter by
     * @return OrderTableModel built from list.
     */
    public OrderTableModel ListOrders(String search) {
        return new OrderTableModel(Order.orderSearch(search));
    }
    
    
    /**
     * Sets the ItemCreatePanel's item based upon the one in the table.
     * @param row of the table to use. Does nothing if out of bounds.
     * @param table to reference.
     */
    public void SetItemCreateEditor(int row, JTable table){
        try{
        
            Object value = table.getModel().getValueAt(row, 1);
            createItemController.setEditorItem((String)value);
            navigate(View.CreateItem);
            
        }
        catch(ArrayIndexOutOfBoundsException | TypeNotPresentException e){}
    }
    
    
    /**
     * Set's the OrderCreatePanel's order based upon one in the table.
     * @param row of the table to use. Does nothing if out of bounds.
     * @param table to reference.
     */
    public void SetOrderCreateEditor(int row, JTable table){
         try{
        
            Object value = table.getModel().getValueAt(row, 1);
            createOrderController.setEditorOrder((String)value);
            navigate(View.CreateOrder);
            
        }
        catch(ArrayIndexOutOfBoundsException | TypeNotPresentException e){}
    }
    
}
