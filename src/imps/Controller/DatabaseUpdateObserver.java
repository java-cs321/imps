/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller;

import java.util.ArrayList;

/**
 * Observer Class that notifies views when the database has changed.
 * @author Michael Colsch
 */
public abstract class DatabaseUpdateObserver {
    
    
    private final ArrayList<DatabaseUpdateListener> listeners;
    
    /**
     * Creates an empty observer
     */
    public DatabaseUpdateObserver(){
        listeners = new ArrayList<>();
    }
    
    /**
     * Adds a listener to the DB update listener.
     * @param l listener added to the observer
     */
    public void addListener(DatabaseUpdateListener l){
        listeners.add(l);
    }
    
    /**
     * Function to notify listeners
     */
    public void dbUpdate(){
        listeners.forEach(l -> {
            l.onDBUpdate();
        });
    }
    
}
