/**
 * Controller Package for imps.
 * Acts as a link between the imps.Models.Storage package and the imps.View
 * packages, but helps separate concerns between them.
 * 
 */
package imps.Controller;
