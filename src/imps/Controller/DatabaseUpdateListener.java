/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller;

/**
 * Listens for database changes.
 * @author Michael Colsch
 */
public interface DatabaseUpdateListener {
    
    /**
     * Update function when DB changes.
     * This function takes no parameters and assumes the inheriting class has access to the data.
     */
    public void onDBUpdate();
    
}
