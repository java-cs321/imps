/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Item;

import imps.Controller.Controller;
import imps.Controller.DatabaseUpdateObserver;
import imps.Model.Item;
import imps.View.Item.ItemEditorPanel;
import imps.Model.ComboBox.ItemComboBox;
import imps.View.MainPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * Controller for the create item card panel
 * @author Michael Colsch
 */
public class CreateItemController extends DatabaseUpdateObserver implements CreateItemUpdateListener {

    //View this controller acts on
    private final ItemEditorPanel view;
    
    //Temporary Item held by this controller.
    private Item item;
    
    //Model for selecting items.
    private final ItemComboBox itemSelectModel;
    
    
    /**
     * Creates a new ItemController and binds fields to the CreateItemView Panel
     * @param viewItemPanel to bind to.
     */
    public CreateItemController(ItemEditorPanel viewItemPanel){
        super();
        //Connect Dropdown to view.
        view = viewItemPanel;
        itemSelectModel = new ItemComboBox();
        itemSelectModel.setItemListeners(view.getItemListeners());
        view.setItemSelectModel(itemSelectModel);
        
        //Update this item with the rest of the view.
        view.getItemListeners().add(this);
        
        //Link submit button to create/update.
        view.setSubmitAction((ActionEvent ae) -> {
            if (submit()){
                JOptionPane.showMessageDialog(view, "Item Successfully saved.");
                Controller.Instance.navigate(MainPanel.View.Catalog);
            }else{
                JOptionPane.showMessageDialog(view, "Failed to save changes");
            }
        });
        
    }
    
    /**
     * Tries to insert or update an item into the DB, built from the view.
     * @return true if successful.
     */
    public boolean submit(){
        
     
        if(item != null){
            //Set fields that don't auto-update.
            item.setPrice(view.getPrice());
            item.setQuantity(view.getQuantity());
            //Update
            try {
                item.setID(Item.getDatabase().CreateItem(item));
                super.dbUpdate();
                return true;
           
           }catch(SQLException e){
               //On failure, try again
               try {
                   int result = Item.getDatabase().UpdateItem(item);
                   if (result >= 0){
                       super.dbUpdate();
                       return true;
                   }
               }catch(SQLException e2){}
           }
        }
        return false;
    }

    /**
     * Ensures this item gets updated with every other item.
     */
    @Override
    public void ItemChanged(Item item) {
        this.item = item;
    }
     
    
    /**
     * Pass through function to set the view's item.
     * @param itemName name of the item being set in the editor.
     */
    public void setEditorItem(String itemName){
        view.setItem(itemName);
    }
    
    
}
