/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Item;
import imps.Model.Item;
import java.util.ArrayList;

/**
 * Abstract Class for objects that modify items.
 * This Object will sends events on itemChanged to Listeners.
 * @author Michael Colsch
 */
public abstract class ItemUpdateObserver {
    
    //Components to update with the item.
    protected ArrayList<CreateItemUpdateListener> itemListeners;
    
    /**
     * Sets the item listeners 
     * @param listeners  that react on item update.
     */
    public void setItemListeners(ArrayList<CreateItemUpdateListener> listeners){
        this.itemListeners = listeners;
    }
    
    /**
     * Creates a new Observer and initializes the set of Listeners to empty.
     */
    public ItemUpdateObserver(){
        this.itemListeners = new ArrayList<>();
    }
    
    /**
     * Updates all listeners attached.
     * @param item the item that is used to update the view.
     */
    protected void itemChanged(Item item) {
        for(CreateItemUpdateListener listener : this.itemListeners){
            listener.ItemChanged(item);
        }
        
    }
    
    
    
}
