/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Item;

import imps.Model.Item;

/**
 * Handles events that update the CreateItemView.
 * @author Michael Colsch
 */
public interface CreateItemUpdateListener {
    
    /**
     * Notify the GUI that the selected item has changed.
     * @param item - the new item being used.
     */
    public void ItemChanged(Item item);
    
    
}




