/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Order;

import imps.Controller.Controller;
import imps.Controller.DatabaseUpdateListener;
import imps.Controller.DatabaseUpdateObserver;
import imps.Model.ComboBox.ItemComboBox;
import imps.Model.ComboBox.OrderComboBox;
import imps.Model.ComboBox.OrderStatusComboBox;
import imps.Model.Item;
import imps.Model.TableModel.ItemTableModel;
import imps.Model.Order;
import imps.View.MainPanel;
import imps.View.Order.OrderEditorPanel;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import javax.swing.JOptionPane;

/**
 * Controller for the create order card panel
 * @author Paul
 * @author Michael
 */
public class CreateOrderController extends DatabaseUpdateObserver implements CreateOrderUpdateListener, DatabaseUpdateListener {
    private final OrderEditorPanel view;
    private Order model;
    
    
    //Vars related to adding items to orders
    private ItemComboBox itemSelectModel;
    private ArrayList<Item> itemsInOrder;
    private ItemTableModel itemsInOrderTable;
    
    
    //Vars related to order selection
    private final OrderStatusComboBox orderStatusModel;
    
    /**
     * Initializes view and controller.
     * @param view to bind to the controller.
     */
    public CreateOrderController(OrderEditorPanel view) {
        this.view = view;
        model = new Order();
        //Item DropDown 
        itemSelectModel = new ItemComboBox();
        view.setItemSelectionModel(itemSelectModel);
        //Item Table
        itemsInOrder = new ArrayList<>();
        itemsInOrderTable = new ItemTableModel(itemsInOrder, false);
        view.setItemInOrderTableModel(itemsInOrderTable);
        //Item Button
        view.setAddItemActionListener((ActionEvent eve) -> {
            addItemToOrder();
        });
        //Submit Button
        view.setSubmitActionListener((ActionEvent eve)-> {
            if (submit()){
                JOptionPane.showMessageDialog(view, "Order Successfully saved.");
                Controller.Instance.navigate(MainPanel.View.Order);
            }else{
                JOptionPane.showMessageDialog(view, "Failed to save changes");
            }
        });
        
        //Order Status
        orderStatusModel = new OrderStatusComboBox();
        view.setOrderStatusModel(orderStatusModel);
        
        //Order DropDown
        OrderComboBox orderSelectionModel = new OrderComboBox();
        view.setOrderSelectionModel(orderSelectionModel);
        
        //Attach Listeners to observers
        ArrayList<CreateOrderUpdateListener> listeners = new ArrayList<>();
        listeners.add(orderStatusModel); //Change the order status
        listeners.add(view); //Make sure the GUI syncs properly
        listeners.add(this); //The only observer is in the last statement, so the this leak is alright.
        //Attach listners to the order select.
        orderSelectionModel.setListeners(listeners);
        
    }
    
    /**
     * Adds an item from the GUI to the table model.
     */
    private void addItemToOrder(){
        String selected = (String)itemSelectModel.getSelectedItem();
        //Do nothing on no selection. 
        if(selected == null || selected.equals("")){
            return;
        }
        
        Item newItem = new Item(selected, view.getItemQuantity(), BigDecimal.ONE);
        
        ArrayList<Item> found = Item.itemSearch(selected);
        if (!found.isEmpty()){
            newItem.setPrice(found.get(0).getPrice());
            newItem.setID(found.get(0).getID());
            //aesthetics
            newItem.setPicture(found.get(0).getPicture());
        }
        
        Optional<Item> filtered =  itemsInOrder.stream().filter(l -> l.getName().equals(selected)).findAny();
        //If the item is present, update it.
        if(filtered.isPresent()){
            filtered.get().addQuantity(newItem.getQuantity());
            if(filtered.get().getQuantity() == 0){
                itemsInOrder.remove(filtered.get());
            }
            
        }else if(newItem.getQuantity() != 0) {
            itemsInOrder.add(newItem);
        }

        itemsInOrderTable = new ItemTableModel(itemsInOrder, false);
        view.setItemInOrderTableModel(itemsInOrderTable);
        
    }

    
    /**
     * Saves an order to the DB.
     * @return true on success
     */
    private boolean submit(){
        
        if(model != null){
            //Set Fields.
            model.setCode(orderStatusModel.getSelectedCode());
            model.setItems(itemsInOrder);
            
            //Create
            try {
                model.setID(Order.getDatabase().CreateOrder(model));
                super.dbUpdate();
                return true;
            }catch(SQLException e){
                //If fail, try updating
                try{
                  int result = Order.getDatabase().UpdateOrder(model);
                  if(result > 0){
                      super.dbUpdate();
                  }
                  return true;
                }catch(SQLException e2){
                    System.out.print(e2);
                }
            }
            
        }
        
        return false;
    }
    
    
    /**
     * Sets the order on the view
     * @param orderName name of the order to set this view to.
     */
    public void setEditorOrder(String orderName){
        view.setOrder(orderName);
    }
    
    /**
     * Updates Table with Items from order.
     * @param order that was updated.
     * Updates the Items in order to avoid creating a decorator on an existing model.
     */
    @Override
    public void onOrderChanged(Order order) {
        model = order; //Update this controller with the current model.
        itemsInOrder = order.getItems();
        //Use the model to update the view.
        itemsInOrderTable = new ItemTableModel(order.getItems(), false);
        view.setItemInOrderTableModel(itemsInOrderTable);
    }

    /**
     * Refreshes the list of items on db update.
     */
    @Override
    public void onDBUpdate() {
        itemSelectModel = new ItemComboBox();
        view.setItemSelectionModel(itemSelectModel);
    }

    
    
    
}
