/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Order;

import imps.Model.Order;

/**
 * Interface for objects that react to order changed events.
 * Listener for OrderUpdateObservers.
 * @author Michael Colsch
 */
public interface CreateOrderUpdateListener {
    
    /**
     * Order update event callback.
     * @param order that was updated.
     */
    public void onOrderChanged(Order order);
}
