/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Controller.Order;

import imps.Model.Order;
import java.util.ArrayList;

/**
 *  Abstract class for objects that modify orders.
 * This Observer will send events onOrderChanged to listeners
 * @author Michael Colsch
 */
public class OrderUpdateObserver {
    
    private ArrayList<CreateOrderUpdateListener> listeners;
    
    
    /**
     * Sets the listeners for this observer
     * @param listeners to bind to this observer.
     */
    public void setListeners(ArrayList<CreateOrderUpdateListener> listeners){
        this.listeners  = listeners;
    }
    
            
    /**
     * Initializes observer
     */        
    public OrderUpdateObserver(){
        listeners = new ArrayList<>();
    }
    
    /**
     * Function that calls the listeners.
     * @param order to broadcast.
     */
    protected void orderChanged(Order order){
        listeners.forEach(l -> l.onOrderChanged(order));
    }
    
}
