/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.ComboBox;

import imps.Controller.Order.CreateOrderUpdateListener;
import imps.Model.Order;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Model for listing the orders of a ComboBox.
 * @author Michael Colsch
 */
public class OrderStatusComboBox implements ComboBoxModel<String>, CreateOrderUpdateListener{

    private ArrayList<String> statuses;
    Map<Integer,String> codeMap;
    private String selected;
    
    /**
     * Creates a model of Order Statuses.
     * Uses both a map and an ArrayList.
     */
    public OrderStatusComboBox(){
        statuses = new ArrayList<>();
        try{
             codeMap = Order.getDatabase().getStatuses();
            //Remap into list.
            codeMap.forEach((k,v)-> statuses.add(v));
        }catch(SQLException e){}
    }
    
    /**
     * Sets the value of the ComboBox
     * @param obj used to set the value.
     * If obj is an Integer, the value is set to the string for the status code.
     * If obj is a String, the value is set to the matching string in the list.
     * Otherwise, nothing happens.
     */
    @Override
    public void setSelectedItem(Object obj) {
        if( obj instanceof Integer){
            selected = codeMap.getOrDefault(obj, selected);
        }else if(obj instanceof String){
            //Set selected to obj if it's in the list.
            Optional<String> found = statuses.stream().filter(s -> s.equals((String)obj)).findAny();
            if (found.isPresent()){
                selected = found.get();
            }
        }
    }

    /**
     * Gets item in ComboBox.
     * @return currently selected guaranteed to be a string.
     * 
     */
    @Override
    public String getSelectedItem() {
        return selected;
    }
    
    /**
     * Gets the status code for the selected item
     * @return the status code for the selected string or 0 if not found.
     */
    public int getSelectedCode(){
        if (selected == null)return 0;
        //Filter out a key from a value.
        Optional<Entry<Integer, String>> found = codeMap.entrySet().stream().filter((Entry<Integer, String> e) -> selected.equals(e.getValue())).findAny();
        
        if (found.isPresent()){
            return found.get().getKey();
        }
        return 0;
    }
    
    /**
     * Returns the size of the statuses in the combobox.
     * @return int size
     */
    @Override
    public int getSize() {
        return statuses.size();
    }

    /**
     * Returns string element at index given
     * @param arg0 index of interest
     * @return string status
     */
    @Override
    public String getElementAt(int arg0) {
       return statuses.get(arg0);
    }

    /**
     * Override for compatibility.
     * @param arg0 only provided for compatibility, does not do anything.
     */
    @Override
    public void addListDataListener(ListDataListener arg0) {
        
    }

    /**
     * Override for compatibility.
     * @param arg0 only provided for compatibility, does not do anything.
     */
    @Override
    public void removeListDataListener(ListDataListener arg0) {
        
    }

    /**
     * Detects when an order is changed. 
     * @param order order that was changed
     */
    @Override
    public void onOrderChanged(Order order) {
        setSelectedItem(order.getCode());
    }
    
}
