/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.ComboBox;

import imps.Controller.Item.CreateItemUpdateListener;
import imps.Controller.Item.ItemUpdateObserver;
import imps.Model.Item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;
import javax.swing.ComboBoxModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Representation of an Item Drop-down menu on the CreateItem View.
 * @author Michael Colsch
 */
public class ItemComboBox  extends ItemUpdateObserver implements ComboBoxModel {
    
    //Current Available items.
    private ArrayList<Item> items;
    //The Current selected item
    private Item selected;
    
    
    
    
    /**
     * Initializes a new ItemComboBox.
     */
    public ItemComboBox(){
        super();
        this.items = Item.itemSearch("");
        
    }
      
    /**
     * Sets the CreateItemUpdateListeners for an item.
     * @param listeners that waits for updates.
     */
    @Override
    public void setItemListeners(ArrayList<CreateItemUpdateListener> listeners){
        itemListeners = listeners;
    }
    
    /**
     * Sets the selected item.
     * @param anItem if anItem is inside the list of Items, then set selected to the item. Otherwise, make a new item.
     */
    @Override
    public void setSelectedItem(Object anItem) {
        items = Item.itemSearch("");
        //Selected Object is actually an item.
        if (anItem instanceof Item){
            //Limit to items with similar names.
            Item item = (Item)anItem;
            //Find the item
            int idx = items.indexOf(anItem);
            if (idx != -1) {
                selected = items.get(idx);
            }
            
        }//Selected Object is a String.
        else if(anItem instanceof String){
            Optional<Item> found =  items.stream().filter(l -> l.getName().equals((String)anItem)).findAny();
            if(found.isPresent()){
               selected = found.get();
            }
            //Item should not be blank.
            else if(!anItem.equals("")) {
                selected = new Item((String)anItem, 0, BigDecimal.ZERO);
            }else {
                selected = null;
            }
        }
        this.itemChanged(selected);
       

        
    }

    /**
     * Retrieves the selected Item's name. Guarantees output is a String.
     * @return the selected item's name or a blank string if either the item or name is null.
     */
    @Override
    public Object getSelectedItem() {
        if (selected == null || selected.getName() == null){
            return "";
        }
        return selected.getName();
    }
    
    /**
     * Returns how many items in combobox.
     * @return int size 
     */
    @Override
    public int getSize() {
        return items.size();
    }
    
    /**
     * Gets the string at the index given.
     * @param index index of interest
     * @return string element of interest
     */
    @Override
    public String getElementAt(int index) {
        if (items.get(index) == null){
            return "";
        }
        return items.get(index).getName();
    }
        
    //Functions for compatiblity with Combobox interface.
    
    /**
     * Override for compatibility.
     * @param l only provided for compatibility, does not do anything.
     */
    @Override
    public void addListDataListener(ListDataListener l) {}

    /**
     * Override for compatibility.
     * @param l only provided for compatibility, does not do anything.
     */
    @Override
    public void removeListDataListener(ListDataListener l) {}

    

    
}
