/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.ComboBox;

import imps.Controller.Order.OrderUpdateObserver;
import imps.Model.Order;
import java.util.ArrayList;
import java.util.Optional;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

/**
 * Model for ComboBox that selects orders.
 * @author Michael Colsch
 */
public class OrderComboBox extends OrderUpdateObserver implements ComboBoxModel{

    //Current Available orders.
    private ArrayList<Order> orders;
    //The Current selected order
    private Order selected;
    
    
    /**
     * Creates a new OrderCombobox.
     */
    public OrderComboBox(){
        orders = Order.orderSearch("");
    }
    
    /**
     * Sets the selected order.
     * @param anOrder either the name of an order, an Order object.
     * If the order name does not exist, a new order will be created.
     */
    @Override
    public void setSelectedItem(Object anOrder) {
        orders = Order.orderSearch("");
        if (anOrder instanceof Order){
            selected = (Order)anOrder;
            int idx = orders.indexOf(anOrder);
            if (idx != -1) {
                selected = orders.get(idx);
            }
        }else if(anOrder instanceof String){
            Optional<Order> found = orders.stream().filter(o -> o.getName().equals(anOrder)).findAny();
            if(found.isPresent()){
                selected = found.get();
            }else if (!anOrder.equals("")){
                selected = new Order((String)anOrder, 1);
            }
        }
        
        this.orderChanged(selected);
    }

    /**
     * Retrieves the name of the selected order.
     * @return name of the order or an empty string. Never null.
     */
    @Override
    public String getSelectedItem() {
        if(selected == null){
            return "";
        }
        return selected.getName();
    }
    
    /**
     * Retrieves the selected order.
     * @return currently selected order.
     */
    public Order getSelectedOrder(){
        return selected;
    }
    
    /**
     * Returns the size of the items in the combobox.
     * @return int size
     */
    @Override
    public int getSize() {
        return orders.size();
    }

    /**
     * Retrieves the name of an element.
     * @param arg index to retrieve the order.
     * @return string of the element name
     */
    @Override
    public String getElementAt(int arg) {
        return orders.get(arg).getName();
    }
    
    /**
     * Override for compatibility.
     * @param arg0 only provided for compatibility, does not do anything.
     */
    @Override
    public void addListDataListener(ListDataListener arg0) {
        
    }

    /**
     * Override for compatibility.
     * @param arg0 only provided for compatibility, does not do anything.
     */
    @Override
    public void removeListDataListener(ListDataListener arg0) {
        
    }
    
}
