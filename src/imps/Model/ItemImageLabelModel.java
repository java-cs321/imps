/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model;

import imps.Controller.Item.ItemUpdateObserver;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


/**
 * Allows images to be set to labels
 * @author Michael Colsch
 */
public class ItemImageLabelModel extends ItemUpdateObserver {
    
    //Frame to be used in dialog
    private Component parent;
    //This image this component last read
    private Image image;
    
    private MouseAdapter adapter;
    
    //Item modified by this object.
    private Item currentItem;
    
    /**
     * Creates an ImageLabelModel using the base Frame and uses title as the title of the dialog window.
     * @param parent - parent object for the dialog.
     */
    public ItemImageLabelModel(Component parent){
        this.parent = parent;
        //Make a mouse adapter set this item then use it.
        adapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent eve){
                //Must select a file to do anything
                if(!selectFile()){
                    return;
                }
                if(currentItem == null){
                    JOptionPane.showMessageDialog(parent, "Error: Select or Create an Item First");
                    return;
                }
                currentItem.setPicture(image);
                itemChanged(currentItem);
            }
        };
    }
    
    
    /**
     * Retrieves the current image.
     * @return image last selected
     */
    public Image getImage(){
        return image;
    }
    
    /**
     * Sets the current item to use as a reference for the model
     * @param item used to set the current.
     */
    public void setItem(Item item){
        currentItem = item;
    }
    
    
    /**
     * Gets the mouse adapter to connect this image system to.
     * @return adapter linked to this file.
     */
    public MouseAdapter getMouseAdapter(){
        return this.adapter;
    }
    
    /**
     * Sets Image based on a file dialog.
     * @return true if an image was set.
     */
    public boolean selectFile(){
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(parent);
        
        if(result ==  JFileChooser.APPROVE_OPTION){
            File file = fileChooser.getSelectedFile();
            try {
                BufferedImage image = ImageIO.read(file);
                this.image = image;
            } catch(IOException e){
                JOptionPane.showMessageDialog(parent, "Error: file could not be read.");
            }
            return true;
        }
        return false;
        
    }
    
    
    
    
    
}
