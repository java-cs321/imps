/**
 * Package containing the data model for imps
 * This Package is split into two sub-packages:
 *
 * The imps.Model.Shared package holds classes containing information that
 * is vital to the core of the inventory management system
 * 
 * The imps.Model.Storage package defines access classes to a storage medium.
 * Any logic to transform classes from the imps.Model.Shared package into data 
 * in a db or vice versa should be in this package.
 */
package imps.Model;
