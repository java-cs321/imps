
package imps.Model.Storage;

import imps.Model.Item;
import imps.Model.Order;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
/**
 * Class that connects to an H2 Database.
 * All DB-Specific actions should go here.
 * All external calls to functions should go through the DBService interface.
 * @author Michael Colsch
 */
public class H2DB implements DBService {

    private String dbName;
    private Connection db;
    
    /**
     * Internal Method for creating a connection.
     * @return Connection to DB.
     * @throws SQLException failed to connected.
     * Protected to allow it to be used or overridden, especially in tests.
     */
    protected Connection newConnection() throws SQLException{
        //The connection tries to use embedded mode but falls back on server mode.
        return DriverManager.getConnection("jdbc:h2:"+dbName+";AUTO_SERVER=TRUE");
    }
    
    /**
     * Creates a schema for an active db connection.
     * @param  db connection to try to create schema on.
     * @throws SQLException schema execution error.
     * @throws IOException cannot read schema file.
     */
    protected void CreateSchema(Connection db)throws SQLException,IOException{
        System.out.println("Creating Schema for db");
        Statement query = db.createStatement();
        File schema = new File("./schema.sql");
       
       FileInputStream fis = new FileInputStream(schema);
       String s = new String(fis.readAllBytes());
       query.execute(s);
    }
    
    /**
     * Seeds the DB at connection db.
     * @throws SQLException seed execution error.
     * @throws IOException cannot read seed file.
     */
    public void Seed() throws SQLException,IOException{
        System.out.println("Seeding db");
        Statement query = db.createStatement();
        File seed = new File("./seed.sql");
       
        FileInputStream fis = new FileInputStream(seed);
        String s = new String(fis.readAllBytes());
        query.execute(s);
        
        
    }

    /**
     * Initializes DB if not present.
     * Tries to connect to DB and verifies the tables in the IMPS Schema.
     * @param name name of the db connection. Expects format host:port for remote
     * connections or path for embedded db.
     * @return True if a new schema was created.
     * @throws SQLException Failed to connect or login to db.
     * @throws IOException  Failed to open db file.
     */
    @Override
    public boolean Init(String name) throws SQLException,IOException {
        dbName = name;
        db = newConnection();
        Statement query = db.createStatement();
        try {
           
           ResultSet results = query.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'IMPS' ORDER BY TABLE_NAME ");
            ArrayList<String> tables = new ArrayList<>();
            tables.add("ITEMS");
            tables.add("ORDERS");
            tables.add("ORDER_ITEM");
            tables.add("ORDER_STATUS");
            //Include Views in tablecount
            tables.add("ORDERDETAIL");
            tables.add("ORDERINFO");
            int count = 0;
            while (results.next()){
                String table = results.getString(1);
               if (!tables.contains(table)) {
                   throw new SQLException();
               }
               count++;
           }
           if (count != tables.size()) {
               throw new SQLException();
           }
           
           
        }catch(SQLException e){
            CreateSchema(db);
            return true;
        }
        
        return false;
                
    }

    
    
    /**
     * Close the active connection for H2DB.
     * Last call to H2DB, closes connection to the database.
     * Opening and closing connections causes massive overhead in windows, so just hold the connection while running.
     */
    public void close(){
    
        try{
            db.close();
        }catch(SQLException e){}
        
    }
    
    
    /**
     * Retrieves a list of items from the db.
     * @return the list of items retrieved.
     */
    @Override
    public ArrayList<Item> GetItemsInInventory() {
        return this.itemSearch("");
    }

    /**
     * Retrieves a list of orders from the db.
     * @return the list of orders retrieved.
     */
    @Override
    public ArrayList<Order> GetOrders() {
        return this.orderSearch("");
    }

    /**
     * Fetches items for a given order.
     * @param orderID to identify which order to get items for.
     * @return list of items for the order or empty list if order is not present.
     */
    @Override
    public ArrayList<Item> GetItemsForOrder(int orderID) {
        ArrayList<Item> items = new ArrayList<>();
        try {
            PreparedStatement query = db.prepareStatement("SELECT * FROM IMPS.ORDERDETAIL WHERE ID = $1");
            query.setInt(1, orderID);
            
            ResultSet results = query.executeQuery();
            
            
            while(results != null && results.next()) {
                Item tmp = new Item(results.getString("ITEM_NAME"), results.getInt("quantity"), results.getBigDecimal("unit_price"));
                tmp.setID(results.getInt("item_id"));
                tmp.setPicture(bytesToImage(results.getBlob("picture")));
                items.add(tmp);
                
            }
            
            
        }catch(SQLException e){
            System.out.print("Warning: Exception caught"+e.getMessage());
        }
        return items;
    }

    
    /**
     * Inserts an item into the DB, creating it from an inventory management standpoint.
     * @param item item inserted into the db.
     * @return id of the last inserted or 0 on failure to retrieve it.
     */
    @Override
    public int CreateItem(Item item) throws SQLException {
        Connection db = newConnection();
        PreparedStatement query = db.prepareStatement("INSERT INTO IMPS.ITEMS (NAME,PRICE,QUANTITY,PICTURE) VALUES (?,?,?,?)");
        query.setString(1, item.getName());
        query.setBigDecimal(2, item.getPrice());
        query.setInt(3, item.getQuantity());
        
        query.setBlob(4, imageToBytes(item.getPicture()));
        
        query.executeUpdate();
        int max = getMaxID(db, Table.Items);
        return max;
    }
    
    /**
     * Transforms an image into a byte stream.
     * @return image as a blob form.
     */
    private ByteArrayInputStream imageToBytes(Image im){
        if (im == null){ return null; }
        //Copy Image to buffer
        BufferedImage buffer = new BufferedImage(im.getWidth(null), im.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = buffer.createGraphics();
        g.drawImage(im, 0, 0,null);
        //Write to byte stream
        ByteArrayOutputStream bytes =  new ByteArrayOutputStream();
        try{
            ImageIO.write(buffer, "png", bytes);
        }catch (IOException e){}
        finally{
            try{ bytes.close();}
            catch(IOException e){}
        }
        //Return Image
        return new ByteArrayInputStream(bytes.toByteArray());
    }
    

    /**
     * Update the item in the DB by ID.
     * @param item used to override the one in the db.
     * @return number of rows updated
     * @throws SQLException update failed
     */
    @Override
    public int UpdateItem(Item item) throws SQLException {
        PreparedStatement query = db.prepareStatement("UPDATE IMPS.ITEMS SET NAME = $1, PRICE = $2, QUANTITY=$3,PICTURE=$4 WHERE ID = $5");
        query.setString(1, item.getName());
        query.setBigDecimal(2, item.getPrice());
        query.setInt(3, item.getQuantity());
        query.setBlob(4, imageToBytes(item.getPicture()));
        
        query.setInt(5, item.getID());
        int ret = query.executeUpdate();
        return ret;
    }
    
    
    /**
     * Create an order in the DB.
     * @param order to create. The order's ID and Items are ignored. Only the name and status code are used.
     * @throws SQLException failed to create order.
     * @return id of the order that was created or 0 on failure to retrieve it.
     */
    @Override
    public int CreateOrder(Order order) throws SQLException {
        PreparedStatement query = db.prepareStatement("INSERT INTO IMPS.ORDERS (NAME,STATUSID) VALUES ($1,$2)");
        query.setString(1, order.getName());
        query.setInt(2, order.getCode());
        query.executeUpdate();
        
        int id = getMaxID(db,Table.Orders);
        
        this.AddItemsToOrder(id, order.getItems());
        return id;
    }
    
    /**
     * Update an order in the DB.
     * Items are included as a part of the updated, but only the number of orders updated is returned.
     * @param order order to update, the order's ID is used to identify it.
     * @return number of rows in the order table updated.
     * @throws SQLException - failed to update.
     */
    @Override
    public int UpdateOrder(Order order) throws SQLException {
        
        //Update Order table
        PreparedStatement query = db.prepareStatement("UPDATE IMPS.ORDERS SET STATUSID = $1 WHERE $2 = ID");
            //Parameters
        query.setInt(1, order.getCode());
            //Condition parameter
        query.setInt(2, order.getID()); 
        
        //Updated row count
        int updated = query.executeUpdate();
        
        //Update ORDER_ITEM table
        this.AddItemsToOrder(order.getID(), order.getItems());
        
        
        return updated;
    }

    /**
     * Adds Items to an Order. Will update already existing items.
     * @param OrderID of order where the items are being added.
     * @param Items to add to the order. Quantities are used to set Quantities for the Order.
     */
    @Override
    public void AddItemsToOrder(int OrderID, ArrayList<Item> Items) throws SQLException {
        
        if (Items.isEmpty()){
            return;
        }
        
        //Delete Previous
        String queryText = "DELETE FROM IMPS.ORDER_ITEM WHERE ORDER_ID = $1";
        PreparedStatement query = db.prepareStatement(queryText);
        query.setInt(1, OrderID);
        query.execute();
        
        //Insert
        queryText = "INSERT INTO IMPS.ORDER_ITEM (ORDER_ID, ITEM_ID, ITEM_QUANTITY)\n";
        queryText +=  " VALUES ($1,$2,$3);";
        query = db.prepareStatement(queryText);
        //Iterate over items added.
        for(Item item : Items){
            query.setInt(1, OrderID);
            query.setInt(2, item.getID());
            query.setInt(3, item.getQuantity());
            query.addBatch();
        }
        query.executeBatch();
    }
    
    /**
     * Retrieves items from the DB.
     * @param search string that filters results by item name. Search is case insensitive.
     */
    @Override
    public ArrayList<Item> itemSearch(String search) {
        ArrayList<Item> itemresults = new ArrayList<>();
       try {
           search = "%" + search + "%";
            //prepare statement for execution
            PreparedStatement S = db.prepareStatement("SELECT * FROM IMPS.ITEMS WHERE NAME ILIKE ?");
            //set parameters
            S.setString(1,search);
            //execute SQL Query
            ResultSet result = S.executeQuery();
            while(result.next()){
                Item item  = new Item(result.getString("name"), result.getInt("quantity"), result.getBigDecimal("price"));
                item.setID(result.getInt("ID"));
                item.setPicture(bytesToImage(result.getBlob("picture")));
                itemresults.add(item);
                
            }
            
        } catch (SQLException ex) {}
       return itemresults;
    }

    /**
     * Turns a blob into an image
     * @param blob blob to be converted
     * @return image
     */
    private Image bytesToImage(Blob blob){
        
        if(blob == null){
            return null;
        }
        
        try {
            InputStream in = blob.getBinaryStream();
            return ImageIO.read(in);
        }catch(IOException | SQLException e){}
        return null;
    }
    
    
    
    /**
     * Retrieves orders from the DB.
     * @param orderName string that filters results by order name. Search is case insensitive.
     */
    @Override
    public ArrayList<Order> orderSearch(String orderName) {
         ArrayList<Order> orderResults = new ArrayList<>();
         try {
             orderName = "%" + orderName + "%";
            
            //prepare statement for execution
            PreparedStatement S = db.prepareStatement("SELECT * FROM IMPS.ORDERS WHERE NAME ILIKE ?");
            //set parameters
            S.setString(1,orderName);
            //execute SQL Query
            ResultSet result = S.executeQuery();
            
             while(result.next()){
                Order o = new Order(result.getString("name"), result.getInt("statusID"));
                o.setID(result.getInt("id"));
                o.setItems(this.GetItemsForOrder(o.getID()));
                orderResults.add(o); 
            }
            
        } catch (SQLException ex) {
            
        }
         return orderResults;
    }

    /**
     * Gets the status string corresponding to the given code.
     * @param code the code of interest
     * @return the string status
     */
    @Override
    public String statusFromCode(int code) {
        String status = null;
        try {
            
            PreparedStatement query = db.prepareStatement("Select * FROM IMPS.ORDER_STATUS WHERE ID = ?");
            query.setInt(1, code);
            ResultSet results = query.executeQuery();
            if (results.next()){
                status = results.getString("Status");
            }

        } catch (SQLException e) {}
        return status;
    }


    /**
     * Retrieves a mapping of Code to Order Status
     * @return Map of Key=Code, Value=Status
     * @throws SQLException db failure.
     */
    @Override
    public Map<Integer,String> getStatuses() throws SQLException{
        
        Statement query =  db.createStatement();
        ResultSet results = query.executeQuery("SELECT * FROM IMPS.ORDER_STATUS");
        
        Map<Integer,String> statusMap = new HashMap<>();
        
        while(results.next()){
            statusMap.put(results.getInt(1), results.getString(2));
        } 
        return statusMap;
    }
    
    /**
     * Enumeration of tables for this class, specifically for the getMaxID function.
     */
    private enum Table {
        Orders,
        Items
    }
    
    /**
     * Retrieve Last Column Inserted from a table.
     * Due to lack of compatibility of H2DB with SQL standards, the highest id is assumed
     * to be the last inserted. 
     * @param conn the current db connection. Assumed not null.
     * @param t the table to use.
     * @return 0 or the max ID column of the specified table. 0 is assumed invalid.
     */
    private int getMaxID(Connection conn,Table t){
        try {
            Statement query = conn.createStatement();
            switch(t){
                case Items:
                    query.execute("SELECT MAX(ID) FROM IMPS.ITEMS");
                    break;
                case Orders:
                    query.execute("SELECT MAX(ID) FROM IMPS.ORDERS");
                    break;
                default:
                    return 0; //Unknown table is invalid.
            }
            ResultSet result = query.getResultSet();
            if(result.next()){
                return result.getInt(1);
            }
        }catch(SQLException e){}
        return 0;
    }
    
    
    
}

