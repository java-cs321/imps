/**
 * Package for Persistence.
 * This package defines the interface for storage mediums and contains a single 
 * implementer using an H2DB database. 
 * 
 * The configuration for this application uses a file named seed.sql to pre-populate the db.
 * Configuration only runs on start-up or if the file imps.mv.db does not exist.
 * 
 * H2DB connections are run in AUTO_SERVER Mode, allowing multiple connections.
 * 
 */
package imps.Model.Storage;
