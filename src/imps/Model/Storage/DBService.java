
package imps.Model.Storage;

import imps.Model.Item;
import imps.Model.Order;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Interface for Communication between DB and other classes.
 * This interface is used so any DB that supports this interface can use this application.
 * 
 * @author Michael Colsch
 */
public interface DBService {
   
    /**
     * Initializes the DB.
     * Ensures DB exists and all tables used by the project.
     * @param name : name of the DB being connected to.
     * @return True if the DB needed to be initialized and was. (Fresh schema).
     * @throws SQLException - db cannot be opened or created.
     * @throws IOException - some exception occurred trying to access schema.
     */
    public boolean Init(String name)throws SQLException,IOException;
    
    
    
    //DB Accessors
    
    /**
     * Looks up the list of items in inventory
     * @return - ArrayList of Items in Inventory
     * @throws java.sql.SQLException - - Exception from failure to read from DB
     */
    public ArrayList<Item> GetItemsInInventory()throws SQLException;
    
    /**
     * Looks up the list of orders in inventory
     * @return - ArrayList of Orders placed
     * @throws java.sql.SQLException - Exception from failure to read from DB
     */
    public ArrayList<Order> GetOrders() throws SQLException;
    
    /**
     * Function to retrieve a list of items for any order
     * @param orderID - the id of the order to retrieve all the items for
     * @return - items for the given OrderID
     * @throws java.sql.SQLException - Exception from failure to read from DB
     */
    public ArrayList<Item> GetItemsForOrder(int orderID) throws SQLException;
    
    //DB Mutators
    
    /**
     * Creates an item in the DB.
     * @param item - item to insert into the database
     * @return id of the created item.
     * @throws java.sql.SQLException - failed to add item
     */
    public int CreateItem(Item item) throws SQLException;
    
    /**
     * Updates an item in the DB.
     * @param  item - item that replaces the one with the same ID in the DB.
     * @return number of rows affected.
     * @throws SQLException - updated failed from constraints or access.
     */
    public int UpdateItem(Item item) throws SQLException;
    
    /**
     * Creates an order in the DB
     * @param order order to insert into the database
     * @return id of created item
     * @throws java.sql.SQLException - failed to add order
     */
    public int CreateOrder(Order order) throws SQLException;
    
    /**
     * Updates an order in the DB.
     * @param order - order that replaces the one with the same ID in the DB.
     * Note that this does not affect items belonging to this order, it only
     * changes the status and/or name.
     * @return number of rows updated.
     * @throws java.sql.SQLException - failed to update due to constraint or access.
     */
    public int UpdateOrder(Order order) throws SQLException;
    
    /**
     * Adds items to an order
     * @param  OrderID order to add items to
     * @param  Items a special set of items to add to order. The quantities on these items reflect the order quantity, not the quantity in inventory.
     * @throws java.sql.SQLException - failed to add items to an order.
     */
    public void AddItemsToOrder(int OrderID, ArrayList<Item> Items)throws SQLException;
    
    
    //Special Logic involving DB.
    
   /**
    * Searches for an item by name
    * @param search search term
    * @return results as a list of items.
    */
    public ArrayList<Item> itemSearch(String search);
    
    /**
     * Searches for an order by name
     * @param search search term
     * @return results as a list of orders.
     */
    public ArrayList<Order> orderSearch(String search);
    
    
    /**
     * Finds a status String based on the code.
     * @param code code to lookup the string for
     * @return the result of the lookup.
     */
    public String statusFromCode(int code);
    
    /**
     * Returns order statuses as a mapping.
     * @return  Map Key=Code, Value=Status Message
     * @throws SQLException db error.
     */
    public Map<Integer,String> getStatuses() throws SQLException;
}
