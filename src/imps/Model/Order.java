package imps.Model;
import imps.Model.Item;
import java.util.ArrayList;
import imps.Model.Storage.DBService;
import java.math.BigDecimal;
/**
 * This class represents an individual order. 
 * It holds an ID, a status string, a status code, a list of items in the order,
 * and it holds the total monetary value of the order.
 * Private members have their own get/set methods.
 * There are search, sort, and total value methods.
 * @author Eli Williams
 * @author Paul Ryan
 * @author Michael Colsch
 */
public class Order {
    private static DBService database;
    private int orderID; // ID number for the order.
    private String name; //name for order.
    private int statusCode; //Status code used for comparison.
    private ArrayList<Item> items; //Items associated with the order.
    private BigDecimal total; //Total price of the order.
    
   /**
    * Creates an order from a name and a status code.
    * @param name - name associated with the order.
    * @param status - code associated with the order status.
    */
    public Order(String name, int status) {
        this.name = name;
        statusCode = status;
        items = new ArrayList<>(); 
    }
    /**
     * By default make sure the list of items is initialized.
     */
    public Order(){
       items = new ArrayList<>(); 
       statusCode = 1;
    }
    
    /**
     * This is the set function for the id member.
     * @param ID new ID for the order.
     */
    public void setID(int ID){
        orderID = ID;
    }
    /**
     * This is the get function for ID.
     * @return a copy of ID
     */
    public int getID(){
        int num = orderID;
        return num;
    }
    
    /**
     * Get name of order
     * @return name of order.
     */
    public String getName(){return name;}
    
    /**
     * Sets the name of the order.
     * @param name string it use as the new name.
     */
    public void setName(String name){ this.name = name; }
    
    /**
     * Get function for total, copies total into a temp variable.
     * @return A copy of the member variable total.
     */
    public BigDecimal getTotal(){
        BigDecimal temp = total;
        return temp;
    }
    /**
     * This is the get function for the ArrayList of items.
     * @return the items for this order
     */
    public ArrayList<Item> getItems(){
        return items;
    }
    /**
     * This is the set items function to a new ArrayList.
     * @param temp This is a new array list of items to replace the order.
     */
    public void setItems(ArrayList<Item> temp){
        items = temp;
    }
    
    /**
     * Sets the order status based off of the code.
     * @param code New code to replace statusCode.
     */
    public void setCode(int code){
        statusCode = code;
    }
    /**
     * This is the get function for the statusCode.
     * @return The Current Order Status Code.
     */
    public int getCode(){
        int code = statusCode;
        return code;
    }
    
    /**
    * Updates the status code for the order.
    * @param code is the status code taken from the database.
    *
    */
    public void updateCode(int code){   
        statusCode = code;
    }
    
    /**
     * This function returns the status string for the user.
     * @return status - copy of statusString.
     */
    public String getStatus(){
       return database.statusFromCode(statusCode);
    }
    /**
     * 
     * @param orderName This is the string used to find the matching orders.
     * @return Array list of orders matching the search string.
     */
     public static ArrayList<Order> orderSearch(String orderName){
        ArrayList<Order> result= new ArrayList<>();
        result = database.orderSearch(orderName);
        return result;
     }
     /**
      * This is the set function for the database.
      * @param db Database the member variable will be set to.
      */
     public static void setDatabase(DBService db){
        database = db;
     }
     /**
      * This returns the database connection.
      * @return A copy of the database.
      */
     public static DBService getDatabase(){
         DBService temp = database;
         return temp;
     }
    /**
     * This function calculates the total value of the order.
     * @return Total monetary value of an order.
     */
    public BigDecimal totalOrderValue(){
        int i = 0;
        total =  new BigDecimal(0);
        Item temp;
            while(i < items.size())
            {
                temp = items.get(i);
                total = total.add(temp.totalValue());
                i++;
            }
            return total;
    }
    /**
     * This function copies the passed in item and then adds it to the array.
     * @param item this is the item object to be added to the array.
     */
    public void addToArray(Item item){
        Item temp = item;
        items.add(temp);
    }
    /**
     * Sorts Orders by their ID in ascending order.
     * @param list - array list that is sorted.
     * @return list - ArrayList sorted by orderID.
    */
    public static ArrayList<Order> orderSortID(ArrayList<Order> list){
        if(list == null){
            return null;
        }
        /**
         * Comparators work in the following fashion
         * Negative return means t less than t1
         * 0 return means t equal to t1
         * Positive return means t greater than t1
         */
        list.sort((t, t1) -> {
            return t.orderID - t1.orderID;
        });
        
        return list;
    }
    
}
