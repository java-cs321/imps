/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.TableModel;

import imps.Model.Item;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.table.AbstractTableModel;
import java.math.BigDecimal;

/**
 * The tableModel for the catalog view. 
 * Allows the JTable to display all the items and their information.
 * @author Malachi
 */
public class ItemTableModel extends AbstractTableModel {
    
    private final List<Item> itemList;
    private final String[] columnNames = new String[]{
        "Image", "Name", "Price", "Quantity", "Total Value"};
    private final Class[] columnClass = new Class[]{
        Icon.class, String.class, Double.class, Integer.class, Double.class};
    private static int lowAmt = 10;
    private boolean isHighlighted = true;
    // public update()
    
    /**
     * Returns the low quantity amount set in this class.
     * @return the low quantity amount
     */
    public static int getLowAmt() {
        return lowAmt;
    }
    
    /**
     * Constructs the ItemTableModel using the given itemList.
     * @param itemList the list of items the table will display
     */
    public ItemTableModel(List<Item> itemList) {
        this.itemList = itemList;
    }
    
    public ItemTableModel(List<Item> itemList, boolean highlight) {
        this.itemList = itemList;
        isHighlighted = highlight;
    }
    /**
     * Used to look up the name of a column given its index.
     * @param column column of interest
     * @return the name of the selected column.
     */
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    /**
     * Used to look up the class of a column given its index.
     * @param columnIndex column of interest
     * @return the class of the selected column.
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }
    
    /**
     * Gets the number of columns.
     * @return the number of columns
     */
    @Override
    public int getColumnCount() {
        return 5;
    }
    
    /**
     * Gets the number of rows.
     * @return the number of rows using the given itemList
     */
    @Override
    public int getRowCount() {
        return itemList.size();
    }
    
    /**
     * This function returns the value of a cell.
     * It allows the table to use the correct renderer depending on the type of the item.
     * Notably, this is where an item's image is highlighted red if low quantity.
     * @param rowIndex row of interest
     * @param columnIndex column of interest
     * @return the object at the location specified
     * @throws ArrayIndexOutOfBoundsException  when an invalid item is chosen
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) throws ArrayIndexOutOfBoundsException  {
        Item row = itemList.get(rowIndex);
        switch (columnIndex){
            case 0:
                if (row.getPicture() == null) {
                    return null;
                }
                else {
                    Image img = row.getPicture();
                    if (row.getQuantity() <= lowAmt && isHighlighted) {
                        BufferedImage buffer = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
                        Graphics2D g = buffer.createGraphics();
                        g.drawImage(img, 0, 0, null);
                        g.setComposite(AlphaComposite.SrcOver.derive(0.4f));
                        g.setColor(Color.RED);
                        g.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
                        g.dispose();
                        img = buffer;
                    }
                    Image newimg = img.getScaledInstance(75, 75,  java.awt.Image.SCALE_SMOOTH);
                    Icon newIcon = new ImageIcon(newimg);
                    return newIcon;
                }
            case 1:
                return row.getName();
            case 2:
               return row.getPrice();
            case 3:
               return row.getQuantity();
            case 4:
                return BigDecimal.valueOf(row.getQuantity()).multiply(row.getPrice());
            default:
                return null;
        }
    }
    
    /**
     * No matter what cell is selected, it will not be editable.
     * @param rowIndex row of interest
     * @param columnIndex column of interest
     * @return false
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}