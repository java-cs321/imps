/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps.Model.TableModel;
import imps.Model.Order;
import java.util.List; 
import javax.swing.table.AbstractTableModel;
import java.math.BigDecimal;

/**
 * The tableModel for the order panel
 * @author Malachi
 */
public class OrderTableModel extends AbstractTableModel {
    
    private final List<Order> orderList;
    private final String[] columnNames = new String[] {
        "Order ID", "Order Name", "Status String", "Order total ($)"};
    private final Class[] columnClass = new Class[]{
        Integer.class,String.class, String.class, BigDecimal.class};
    
    /**
     * Constructs a new OrderTableModel
     * @param orderList a list of orders to be displayed in the table
     */
    public OrderTableModel(List<Order> orderList) {
        this.orderList = orderList;
    }
    
    /**
     * Returns the name of the selected column
     * @param column column of interest
     * @return a string name
     */
    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
    /**
     * Returns the class of the selected column
     * @param columnIndex column of interest
     * @return a class
     */
    @Override 
    public Class<?> getColumnClass(int columnIndex) {
        return columnClass[columnIndex];
    }
    
    /**
     * Gets the number of columns.
     * @return int number of columns
     */
    @Override
    public int getColumnCount() {
        return 4;
    }
    
    /**
     * Gets the number of rows.
     * @return int number of rows
     */
    @Override
    public int getRowCount() {
        return orderList.size();
    }
    
    /**
     * This function returns the value of a cell.
     * It allows the table to use the correct renderer depending on the type of the item.
     * Notably, this is where an item's image is highlighted red if low quantity.
     * @param rowIndex row of interest
     * @param columnIndex column of interest
     * @return the object at the location specified
     * @throws ArrayIndexOutOfBoundsException  when an invalid order is chosen
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) throws ArrayIndexOutOfBoundsException {
        Order row = orderList.get(rowIndex);
        
        switch(columnIndex){
            case 0:
                return row.getID();
            case 1:
                return row.getName();
            case 2:
                return row.getStatus();
            case 3:
                return row.totalOrderValue();
                
            default:
                return null;
        }
    }
    
    /**
     * Always returns false to ensure displayed results are not edited.
     * @param rowIndex row of interest
     * @param columnIndex column of interest
     * @return false
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
}
