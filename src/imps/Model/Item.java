package imps.Model;
import java.util.ArrayList;
import imps.Model.Storage.DBService;
import java.awt.Image;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
/** 
 * The Item class represents an item in the database.
 * The Item class stores a picture, price, quantity, and name.
 * Private variables have their own get/set functions.
 * Search, sort, and total value functions are implemented. 
 * @author Eli Williams
 * @author  Michael Colsch
 */
public class Item {
    /**
     * local variable for the image class
     */
    
    //DB Identifier.
    private int ID;
    //Visual for the item.
    private Image picture;
    //cost of item
    private BigDecimal price;
    //number of items 
    private int quantity;
    //name of item
    private String name = "";
    //connection to database functions
    private static DBService database;
   /**
    * Default Constructor. Sets Price to 0.
    */
    Item() {
        price = new BigDecimal(0);
    }
    /**
     * Constructor that sets some basic fields.
     * @param name the name of the item.
     * @param units the quantity of the item in inventory
     * @param price the cost per single unit of an item.
     */
    public Item(String name, int units, BigDecimal price) {
        this.price = price;
        this.quantity = units;
        this.name = name;
       
    }
    
    /**
     * Get the DB ID for the Item.
     * @return integer ID of the item. 0 means the item is not in the DB.
     */
    public int getID(){ return ID; }
    
    /**
     * Sets the Item's ID for the DB.
     * @param id integer to use as the new ID.
     */
    public void setID(int id){ ID = id;}
    
    /**
     * Get the Item's name.
     * @return A copy of the item's name.
     */
    public String getName(){ return name; }
    /**
     * This is the set function for the item name.
     * @param temp The new item name to replace the old one.
     */
    public void setName(String temp){ name = temp; }
    /**
     * This function allows the user to set the price.
     * @param a temporary value to change the cost of the price.
     */
    public void setPrice(BigDecimal a){ price = a; }
    /**
     * This function returns the price of the item.
     * @return locally stored price of the item.
     */
    public BigDecimal getPrice(){ return new BigDecimal(price.toString()); }
    /**
     * Allows the user to set the quantity.
     * @param a the new units passed into the method.
     */
    public void setQuantity(int a){ quantity = a; }
    /**
     * This function adds to the quantity.
     * @param add This is the number of units added to the quantity.
     */
    public void addQuantity(int add){ quantity+=add; }
    /**
    *Removes items from the quantity.
    * @param minus This is the number of units to be subtracted. 
    */
    public void removeQuantity(int minus){ quantity-=minus; }
    /**
    * Returns the quantity of items.
    * @return member variable quantity.
    */
    public int getQuantity(){ return quantity; }
    /**
     * This function calculates the monetary value of all the items.
     * @return price multiplied by the quantity of items.
     */
    public BigDecimal totalValue(){ 
        return price.multiply(new BigDecimal(quantity)); 
    }
    /**
     * Retrieves an image of the item.
     * @return Image object containing the picture.
     */
    public Image getPicture(){ return picture; }
    /**
     * Sets the item's picture to an image.
     * @param image image that the picture is set to.
     */
    public void setPicture(Image image){
        picture = image;
    }
    
    //Static Functions.
    
    /**
     * This function searches the database for all items matching the string passed in.
     * @param search This is the item we are searching for in the list.
     * @return the ArrayList that is sorted in the db function.
     */
    public static ArrayList<Item> itemSearch(String search){
        ArrayList<Item> result; 
        result = database.itemSearch(search);
        return result;
    }
    
    /**
     * Specifies how to sort items for the itemSort method.
     */
    public static enum SortBy{
        /**
         * Sort Alphabetically.
         */
        NAME,
        /**
         * Sort by Price.
         */
        PRICE
    };
    
    /**
     * Sorts a list of items alphabetically.
     * @param list to sort.
     * @return sorted list, distinct from the list passed in.
     * This function is identical to itemSort(list,SortBy.Name);
     */
    public static ArrayList<Item> itemSort(ArrayList<Item> list) {
        return itemSort(list,SortBy.NAME);
    }
    
    /**
     * Sorts a list items.
     * @param list to sort.
     * @param sortby defines how the list is sorted.
     * @return sorted list, distinct from the list passed in.
     */
    public static ArrayList<Item> itemSort(ArrayList<Item> list,SortBy sortby){
        //Copy to ensure list passed in is not changed.
        list = new ArrayList<>(list);
        
        switch (sortby){
            case NAME:
                list.sort((t,t1)->{
                    return t.name.compareTo(t1.name);
                });
                break;
            case PRICE:
                list.sort((t,t1)->{
                    return t.price.compareTo(t1.price);
                });
                break;
        }
        
        
        return list;
    }
    
    /**
     * This function accepts a database and initializes the local database.
     * @param db Establishes the database.
     */
    public static void setDatabase(DBService db){
        database = db;
    }
    
    /**
     * Retrieves the DB service related to items.
     * @return Service used for updating items.
     */
    public static DBService getDatabase(){
        return database;
    }
}

