/**
 * Main package for the Inventory Management Process System.
 * Bootstraps the application and runs any initialization procedures.
 */
package imps;
