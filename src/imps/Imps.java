/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imps;

import imps.Controller.Controller;
import imps.Model.Item;
import imps.Model.Order;
import imps.Model.Storage.H2DB;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * BootStrap class for Imps.
 * Initializes the DB, Main Controller, and Window.
 * @author Michael Colsch
 */
public class Imps {

    /**
     * Initializes Imps and starts the program.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //All data is stored in the DB, so make it first
        H2DB db = Imps.makeDB();
        
        if (db == null) {
            System.out.println("Failed to connect to db. Terminating.");
            return;
        }
        
        //Link the DB to the Model
        Item.setDatabase(db);
        Order.setDatabase(db);
        
        //The View depends on the Controller, so make the controller.
        Controller.Instance = new Controller();
        
        
        //Init the GUI.
        imps.View.MainFrame myFrame = new imps.View.MainFrame();
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myFrame.setVisible(true);
        myFrame.setSize(800, 600);
        
        //Close the DB When the app ends. 
        //Since this is an embedded db by default, holding connections is not a problem.
        //The connections are held to avoid slowdown from restablishing connection.s
        myFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent eve){
                try {
                    db.close ();
                } finally {}
            }
        });
        
        
    }
    
    /**
     * Creates or Loads a DB for Imps
     */
    private static H2DB makeDB(){
        H2DB db = new H2DB();
        try {
           if(db.Init("./imps")){
               Imps.SeedDB(db);
           
           }
        }catch (IOException | SQLException e){
            System.out.println(e);
            return null;
        }
        return db;
    }
    
    /**
     * Safely tries to seed the DB
     * File seed.sql is read as a configuration file for new databases.
     */
    private static void SeedDB(H2DB db){
        try {
            int result = JOptionPane.showConfirmDialog(null, "Welcome to Imps! We see you don't have a database yet. Seed the db?", "DB Init", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION){
                db.Seed();
            }
            
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
}
