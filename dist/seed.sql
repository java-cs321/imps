/**
 * Author:  Michael Colsch
 * Created: Nov 5, 2020
 * This file provides sample data to seed the database for imps.
 * Precondition:
 *          This SQL file must be executed in the project root directory.
 *          If using Netbeans, open netbeans in the project root directory 
            then run this file. 
 * Resources contains images of objects.
 * Images are public domain sourced.
 */

--Create Items in inventory
INSERT INTO IMPS.ITEMS ("NAME",PRICE,QUANTITY,PICTURE)
VALUES
('Screws',0.10,500,FILE_READ('./resource/machine-screw.jpg')),
('Nails',0.05,500,FILE_READ('./resource/nail.jpg')),
('Copper Wire 5ft',5.05,100,FILE_READ('./resource/copper-wire.jpg')),
('I-Beams',20.0,2,null)
;


--Orders to get more items
INSERT INTO IMPS.ORDERS ("NAME",STATUSID) VALUES
('Bulk Screws',1),
('Building Kit',2)
;

--Link Screws to the Order for Screws
INSERT INTO IMPS.ORDER_ITEM (ORDER_ID,ITEM_ID,ITEM_QUANTITY)
VALUES
(1,1,5000),
    --Make Building Kit have Screws, Nails, and I-Beams.
(2,1,200),
(2,2,200),
(2,4,100)
;
