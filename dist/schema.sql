/**
 * Author:  Michael Colsch
 * Created: Oct 20, 2020
 * This file is a dependency of the main imps application.
 * Invalid Schemas will be overwritten with the schema in this file.
 */

DROP SCHEMA IF EXISTS  IMPS  CASCADE;
--Schema name
CREATE SCHEMA IMPS;

--Maps to Item objects
CREATE TABLE IMPS.ITEMS(
 id INT AUTO_INCREMENT  PRIMARY  KEY,
name VARCHAR(255) unique not null,
price DECIMAL(100,2),
quantity INT not null default 0,
picture IMAGE
);
--List of Order Statuses, allows messages to change without changing id.
CREATE TABLE IMPS.ORDER_STATUS(
	id INT AUTO_INCREMENT PRIMARY KEY,
	status VARCHAR(255) unique
);
--Maps to Order objects
CREATE TABLE IMPS.ORDERS(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) unique not null,
statusID INT
);
--Ensure Order Statuses match to ids
ALTER TABLE IMPS.ORDERS
	ADD FOREIGN KEY(statusID)
	REFERENCES IMPS.ORDER_STATUS(id);

--Relates items to the orders they are a part of and vice-versa.
--Defines how many items are part of that order
CREATE TABLE IMPS.ORDER_ITEM(
  item_id  int,
  order_id int,
  item_quantity int
);
--Avoid duplicate linking
CREATE UNIQUE INDEX PK_LINK ON IMPS.ORDER_ITEM(
	item_id,order_id
);	
--Each items must exist in the items table
ALTER TABLE IMPS.ORDER_ITEM
    ADD FOREIGN KEY (item_id) 
    REFERENCES  IMPS.ITEMS(id);
ALTER TABLE IMPS.ORDER_ITEM
  ADD FOREIGN KEY(order_id)
REFERENCES IMPS.ORDERS(id);


--Views for helping linking

--Gets the Details of an order. Best used with a filter on the order.
CREATE VIEW IMPS.OrderDetail AS (
    SELECT 
        ord.ID as ID,
        ord."NAME" as ORDER_NAME,
        it."NAME" as ITEM_NAME,
        lnk.ITEM_QUANTITY  as quantity,
        it.PRICE as unit_price,
        it.PICTURE as picture,
        it.ID as item_id,
        stat.STATUS,
        ord.STATUSID
        

    FROM IMPS.ORDERS as ord
    Join IMPS.ORDER_STATUS as stat on stat.ID = ord.STATUSID
    Join IMPS.ORDER_ITEM as lnk on lnk.ORDER_ID = ord.ID
    Join IMPS.ITEMS as it on it.ID = lnk.ITEM_ID
    --This ordering groups multiple rows of the same order together
    ORDER by ORDER_NAME
);


--Basic Information about Orders. Easy to tabularize.
 CREATE VIEW IMPS.OrderInfo AS (
    SELECT 
        ord.ID as ID,
        ord."NAME" as ORDER_NAME,
        sum(it.PRICE*lnk.ITEM_QUANTITY) as price,
        stat.STATUS,
        count(it."NAME") as UNIQUE_ITEMS,
        sum(lnk.ITEM_QUANTITY)  as TOTAL_QUANTITY,
        ord.STATUSID

        FROM IMPS.ORDERS as ord
        Join IMPS.ORDER_STATUS as stat on stat.ID = ord.STATUSID
        Join IMPS.ORDER_ITEM as lnk on lnk.ORDER_ID = ord.ID
        Join IMPS.ITEMS as it on it.ID = lnk.ITEM_ID
        GROUP BY (ORDER_NAME)
);


--Create Statuses for orders, required for program to function.
INSERT INTO IMPS.ORDER_STATUS (STATUS) VALUES 
('Pending'), ('In Transit'), ('Complete'), ('Canceled')
;