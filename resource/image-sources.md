# Images Used

[copper-wire.jpg](https://www.shutterstock.com/image-photo/close-copper-wire-raw-materials-metals-786273925)

![](https://image.shutterstock.com/image-photo/closeup-copper-wire-raw-materials-600w-786273925.jpg) 

[machine-screw.jpg](https://shotstash.com/photo/screws-macro/)
![](https://shotstash.com/wp-content/uploads/2019/10/shot-stash-macro-screws-carpentry-1100x733.jpg)


[nail.jpg](https://negativespace.co/carpentry-nails/)

![](https://negativespace.co/wp-content/uploads/2019/04/negative-space-macro-shiny-nails-woodwork-1062x708.jpg)


[stone.jpg](https://commons.wikimedia.org/wiki/File:Wonder_Lake_and_Denali.jpg)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Wonder_Lake_and_Denali.jpg/1167px-Wonder_Lake_and_Denali.jpg)
