# IMPS README

## IMPS Team 8
IMPS was developed by Michael Colsch, Eli Williams, Paul Ryan, and Malachi Landis for CS321 during the Fall 2020 semester at the University of Alabama in Huntsville.

## About IMPS
IMPS stands for the Inventory Management Production System. This software allows the user to store data on items and create orders for sets of items. 

## Using IMPS
Please see our documentation for a detailed use-case to follow. It is important that you read the documentation before beginning to use IMPS. 

## Necessary Libraries
IMPS makes use of JUnit4 and H2DB.

## License
Copyright (c) 2020 Michael Colsch, Eli Williams, Paul Ryan, Malachi Landis

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
